import java.awt.event.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;

/**
 * @author Samantha Bond
 */
public class LyricFinderForm extends JFrame {

    DefaultComboBoxModel model;
    LyricDataOperations lyricDataOperations = new LyricDataOperations();
    ArrayList<String> songList;


    public LyricFinderForm() {
        initComponents();
        createSongModel();
    }

    //creates model to be used with ComboBox to display song names
    public void createSongModel(){
        songList = getSongList();
        model = new DefaultComboBoxModel();
        for (String song : songList){
            model.addElement(song);
        }
        cboSong.setModel(model);
    }

    //returns ArrayList of the url's of the songs
    public ArrayList<String> getSongList(){
        File file = new File("//Users//samanthabond//Desktop//Course Work//Lyrics.txt");
        StringBuilder builder = new StringBuilder();
        try(FileReader reader = new FileReader(file)){
            int ch;
            while ((ch = reader.read()) != -1){
                builder.append((char)ch);
            }
        }
        catch (IOException ioe){
            ioe.toString();
        }
        songList = new ArrayList<>(Arrays.asList(builder.toString().split(",")));
        return songList;
    }


    private void btnCloseActionPerformed(ActionEvent e) {
        this.dispose();
    }

    //when a song is chosen, retrieves lyric data of selected item in combobox
    // and sets the text in the frame to the formatted lyrics
    private void cboSongItemStateChanged(ItemEvent e) {
        lyricDataOperations.getLyricData(String.valueOf(cboSong.getSelectedItem()));
        txtLyrics.setText(lyricDataOperations.formatOutput(LyricDataOperations.getLyrics()));
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Samantha Bond
        lblLyricFinder = new JLabel();
        label1 = new JLabel();
        label2 = new JLabel();
        scrollPane1 = new JScrollPane();
        txtLyrics = new JTextArea();
        textArea1 = new JTextArea();
        btnClose = new JButton();
        cboSong = new JComboBox();

        //======== this ========
        setResizable(false);
        Container contentPane = getContentPane();

        //---- lblLyricFinder ----
        lblLyricFinder.setText("Lyric Finder");
        lblLyricFinder.setFont(new Font("Geneva", Font.BOLD, 30));
        lblLyricFinder.setHorizontalAlignment(SwingConstants.CENTER);

        //---- label1 ----
        label1.setText("Song Title: ");
        label1.setFont(new Font("Geneva", Font.PLAIN, 16));

        //---- label2 ----
        label2.setText("Song Lyrics: ");
        label2.setFont(new Font("Geneva", Font.PLAIN, 16));

        //======== scrollPane1 ========
        {
            scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

            //---- txtLyrics ----
            txtLyrics.setEditable(false);
            scrollPane1.setViewportView(txtLyrics);
        }

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.addActionListener(e -> btnCloseActionPerformed(e));

        //---- cboSong ----
        cboSong.addItemListener(e -> cboSongItemStateChanged(e));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblLyricFinder, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
                    .addContainerGap())
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(39, 39, 39)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 503, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(textArea1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(30, Short.MAX_VALUE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label1)
                                    .addGap(18, 18, 18)
                                    .addComponent(cboSong, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGap(425, 425, 425)
                                    .addComponent(btnClose))
                                .addComponent(label2))
                            .addGap(0, 36, Short.MAX_VALUE))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addComponent(lblLyricFinder)
                    .addGap(36, 36, 36)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label1)
                        .addComponent(cboSong, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addComponent(label2)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(24, 24, 24)
                            .addComponent(textArea1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 367, GroupLayout.PREFERRED_SIZE)))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnClose)
                    .addContainerGap(25, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Samantha Bond
    private JLabel lblLyricFinder;
    private JLabel label1;
    private JLabel label2;
    private JScrollPane scrollPane1;
    private JTextArea txtLyrics;
    private JTextArea textArea1;
    private JButton btnClose;
    private JComboBox cboSong;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
