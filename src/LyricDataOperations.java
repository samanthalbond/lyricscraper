import com.sun.xml.internal.bind.v2.TODO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import java.io.*;
import java.util.Dictionary;

public class LyricDataOperations {


    protected static Document doc;
    private static Elements lyrics = null;

    public static Elements getLyrics() {
        return lyrics;
    }


    //takes html containing text of chosen song and formats it to be displayed in the LyricFinderForm frame
    public String formatOutput(Elements html){
        Document document = Jsoup.parse(String.valueOf(html));
        document.outputSettings(new Document.OutputSettings().prettyPrint(false));
        document.select("br").append("");
        String s = document.html().replaceAll("\\\\n", "\n").replaceAll("\"", "");
        return Jsoup.clean(s, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
    }

    //takes given url and sets "lyrics" variable to the html containing the lyrics of chosen song
    public static void getLyricData(String url){
        try {
            doc = Jsoup.connect(url).get();
            lyrics = doc.select("body > div.container.main-page > div > div.col-xs-12.col-lg-8.text-center > div:nth-child(8)");
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    //TODO use Dictionary instead of ArrayList to store urls and song titles. (So titles can be used in combo box)
}
